This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Build Docker Container

```bash
docker build -t innoscript_cezar_terzian:dev .
```

## Run The Docker Container

```bash
docker run --publish 3000:3000 innoscript_cezar_terzian:dev
```

## Access the application

Open the browser and navigate to [http://localhost:3000](http://localhost:3000)
