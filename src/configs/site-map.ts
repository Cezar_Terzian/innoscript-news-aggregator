const sm = {
  the_guardian: {
    url: `/the-guardian`,
  },
  new_york_times: {
    url: `/new-york-times`,
  },
} as const;

export default sm;
