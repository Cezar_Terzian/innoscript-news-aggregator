export const APP_NAME = "Innoscripta News Aggregator";

export const APP_URL = "https://innoscripta-news-aggregator.vercel.app";

export const APP_META_TITLE = `${APP_NAME}`;

export const APP_META_DESCRIPTION = ``;

export const APP_META_KEYWORDS = ``;
