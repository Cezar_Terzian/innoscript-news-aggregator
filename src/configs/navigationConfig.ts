import sm from "./site-map";

export interface NavItem {
  title: string;
  navLink: string;
  as?: "button";
  btnVariant?: "primary" | "outlined";
}

export const commonNavigationConfig: NavItem[] = [
  {
    navLink: "/",
    title: "News API",
  },
  {
    navLink: sm.the_guardian.url,
    title: "The Guardian",
  },
  {
    navLink: sm.new_york_times.url,
    title: "New York Times",
  },
];

export const needAuthNavigationConfig: NavItem[] = [];
export const notNeedAuthNavigationConfig: NavItem[] = [];
