export const companyLinks = [
  {
    title: "About Us",
    link: "/about",
  },
  {
    title: "Contact Us",
    link: "/contact",
  },
  {
    title: "Our Team",
    link: "/team",
  },
  {
    title: "Cities",
    link: "/cities",
  },
  {
    title: "Privacy Policy",
    link: "/privacy-policy",
  },
  {
    title: "Terms of Service",
    link: "/terms",
  },
];
export const links = [
  {
    title: "Home",
    link: "/",
  },
  {
    title: "Book - Get Started",
    link: "/book/get-started",
  },
  {
    title: "Become a Mover",
    link: "/more-info/movers",
  },
  {
    title: "For Individuals",
    link: "/more-info/individuals",
  },
  {
    title: "FAQ",
    link: "/faq",
  },
];
