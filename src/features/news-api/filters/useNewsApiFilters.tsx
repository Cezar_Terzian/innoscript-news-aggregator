import React from "react";
import { useGetSources } from "../api";

const useNewsApiFilters = () => {
  // Search
  const [search, setSearch] = React.useState("");

  // Sources
  const [selectedSources, setSelectedSources] = React.useState([]);
  const { data: sourcesData } = useGetSources();
  const sources = React.useMemo(
    () =>
      sourcesData?.sources.map((sec: any) => ({
        label: sec.name,
        value: sec.id,
      })) ?? [],
    [sourcesData]
  );

  // Dates
  const [fromDate, setFromDate] = React.useState("");
  const [toDate, setToDate] = React.useState("");

  return React.useMemo(
    () => ({
      search,
      setSearch,

      sources,
      selectedSources,
      setSelectedSources,

      fromDate,
      setFromDate,
      toDate,
      setToDate,
    }),
    [
      search,
      setSearch,
      sources,
      selectedSources,
      setSelectedSources,
      fromDate,
      setFromDate,
      toDate,
      setToDate,
    ]
  );
};

export default useNewsApiFilters;
