import React from "react";

import { useGetArticles } from "./api";
import styles from "./styles/the-guardian-styles.module.scss";
import ArticleCard from "@/components/ArticleCard";
import { formatDate } from "@/utility/date";
import MoonLoader from "react-spinners/MoonLoader";
import useNewsApiFilters from "./filters/useNewsApiFilters";
import SearchFilter from "../filters/SearchFilter";
import MultiSelect from "../filters/MultiSelect";
import DateInput from "../filters/DateInput";

const Articles = () => {
  const {
    search,
    setSearch,

    sources,
    selectedSources,
    setSelectedSources,

    fromDate,
    setFromDate,
    toDate,
    setToDate,
  } = useNewsApiFilters();

  const { data, isLoading, isError, isSuccess } = useGetArticles({
    page: 1,
    q: search,
    //@ts-ignore
    sources: selectedSources.map((opt) => opt.value).join(","),

    from: fromDate,
    to: toDate,
  });

  return (
    <div className={styles.section}>
      <div>
        <h6 style={{ marginBottom: ".25rem" }}>Filters</h6>
        <SearchFilter onChange={setSearch} />
        <div style={{ minHeight: "1rem" }}></div>
        <MultiSelect
          //@ts-ignore
          value={selectedSources}
          //@ts-ignore
          onChange={setSelectedSources}
          options={sources}
          label="Source"
        />

        <div style={{ minHeight: "1rem" }}></div>
        <label>From</label>
        <DateInput value={fromDate} onChange={setFromDate} />
        <label>To</label>
        <DateInput value={toDate} onChange={setToDate} />
      </div>
      {!search || !selectedSources.length ? (
        <div className={styles.loading} style={{ minHeight: "10rem" }}>
          <h6>Type your keyword and select a source to see results</h6>
        </div>
      ) : (
        <>
          {isLoading && (
            <div className={styles.loading}>
              <MoonLoader size={42} />
            </div>
          )}
          {isError && (
            <div className={styles.loading}>
              <p style={{ color: "red" }}>An Error Occured</p>
            </div>
          )}
          {isSuccess && (
            <div>
              <h6 style={{ marginBottom: ".25rem" }}>Articles - News API</h6>
              <div className={styles.articles_container}>
                {data?.articles?.map((article: any) => (
                  <ArticleCard
                    key={article.url}
                    title={article.title}
                    date={formatDate(article.publishedAt)}
                    category={article.source.name}
                    link={article.url}
                    image={article.urlToImage}
                  />
                ))}
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default Articles;
