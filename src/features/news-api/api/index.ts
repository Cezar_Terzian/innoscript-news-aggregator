import client from "@/api/client";
import { useQuery } from "@tanstack/react-query";

const defaultPageSize = 45;
const API_KEY = process.env.NEXT_PUBLIC_NEWS_API_KEY!;

const ENDPOINT = {
  ALL: "https://newsapi.org/v2/everything",
  SOURCES: "https://newsapi.org/v2/top-headlines/sources",
};

type Params = {
  page: number;
  q?: string;
  sources?: string;
  from?: string;
  to?: string;
};

export const useGetArticles = (params: Params) => {
  return useQuery({
    queryKey: ["NEWS_API_ARTICLES", params],
    queryFn: async () => {
      const { data } = await client.get(ENDPOINT.ALL, {
        params: {
          apiKey: API_KEY,
          pageSize: defaultPageSize,
          page: params.page,
          sources: params.sources,
          keyword: params.q,
          from: params.from,
          to: params.to,
        },
      });
      return data;
    },
    enabled: params.q !== "" && params.sources !== "",
  });
};

export const useGetSources = () => {
  return useQuery({
    queryKey: ["NEWS_API_SOURCES"],
    queryFn: async () => {
      const { data } = await client.get(ENDPOINT.SOURCES, {
        params: {
          apiKey: API_KEY,
        },
      });
      return data;
    },
  });
};
