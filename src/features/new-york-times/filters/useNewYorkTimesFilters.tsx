import React from "react";

const useNewYorkTimesFilters = () => {
  // Search
  const [search, setSearch] = React.useState("");

  // Dates
  const [fromDate, setFromDate] = React.useState("");
  const [toDate, setToDate] = React.useState("");

  return React.useMemo(
    () => ({
      search,
      setSearch,

      fromDate,
      setFromDate,
      toDate,
      setToDate,
    }),
    [search, setSearch, fromDate, setFromDate, toDate, setToDate]
  );
};

export default useNewYorkTimesFilters;
