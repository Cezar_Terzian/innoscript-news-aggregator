import client from "@/api/client";
import { useQuery } from "@tanstack/react-query";

// const defaultPageSize = 45;
const API_KEY = process.env.NEXT_PUBLIC_NEW_YORK_TIMES_KEY!;

const ENDPOINT = {
  ALL: "https://api.nytimes.com/svc/search/v2/articlesearch.json",
};

type Params = {
  page: number;
  q?: string;
  from?: string;
  to?: string;
};

export const useGetArticles = (params: Params) => {
  return useQuery({
    queryKey: ["NEW_YORK_TIMES_ARTICLES", params],
    queryFn: async () => {
      const { data } = await client.get(ENDPOINT.ALL, {
        params: {
          ["api-key"]: API_KEY,
          page: params.page,
          q: params.q,
          begin_date: params.from || null,
          end_date: params.to || null,
        },
      });
      return data;
    },
  });
};
