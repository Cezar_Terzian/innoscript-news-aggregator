import React from "react";

import { useGetArticles } from "./api";
import styles from "./styles/the-guardian-styles.module.scss";
import ArticleCard from "@/components/ArticleCard";
import { formatDate } from "@/utility/date";
import MoonLoader from "react-spinners/MoonLoader";
import useNewYorkTimesFilters from "./filters/useNewYorkTimesFilters";
import SearchFilter from "../filters/SearchFilter";
import DateInput from "../filters/DateInput";

const Articles = () => {
  const {
    search,
    setSearch,

    fromDate,
    setFromDate,
    toDate,
    setToDate,
  } = useNewYorkTimesFilters();

  const { data, isLoading, isError, isSuccess } = useGetArticles({
    page: 1,
    q: search,

    from: fromDate,
    to: toDate,
  });

  return (
    <div className={styles.section}>
      <div>
        <h6 style={{ marginBottom: ".25rem" }}>Filters</h6>
        <SearchFilter onChange={setSearch} />

        <div style={{ minHeight: "1rem" }}></div>
        <label>From</label>
        <DateInput value={fromDate} onChange={setFromDate} />
        <label>To</label>
        <DateInput value={toDate} onChange={setToDate} />
      </div>
      {isLoading && (
        <div className={styles.loading}>
          <MoonLoader size={42} />
        </div>
      )}
      {isError && (
        <div className={styles.loading}>
          <p style={{ color: "red" }}>An Error Occured</p>
        </div>
      )}
      {isSuccess && (
        <div>
          <h6 style={{ marginBottom: ".25rem" }}>Articles - New York Times</h6>
          <div className={styles.articles_container}>
            {data?.response?.docs?.map((article: any) => (
              <ArticleCard
                key={article._id}
                title={article.headline?.main}
                date={formatDate(article.pub_date)}
                // category={article.sectionName}
                link={article.web_url}
              />
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default Articles;
