import React from "react";
import ReactSelect from "react-select";

type Option = {
  label: string;
  value: string;
};

type IProps = {
  value: string;
  onChange: (newValue: string) => void;
  options: Option[];
  label: string;
};

const Select = ({ value, onChange, options, label }: IProps) => {
  return (
    <>
      <label style={{ fontSize: ".85em" }}>{label}</label>
      <ReactSelect
        options={options}
        value={options.find((option) => option.value === value) ?? ""}
        // @ts-ignore
        onChange={(option) => onChange(option?.value)}
      />
    </>
  );
};

export default Select;
