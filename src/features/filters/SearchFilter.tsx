import React from "react";
import { useDebounce } from "@/hooks/useDebounce";
import styles from "./Filters.module.scss";

type IProps = {
  onChange: (newValue: string) => void;
};

const SearchFilter = ({ onChange }: IProps) => {
  const [value, setValue] = React.useState("");
  useDebounce(() => onChange(value), 700, [value]);

  return (
    <input
      className={styles.input}
      type="text"
      value={value}
      onChange={(e) => setValue(e.target.value)}
      placeholder="Search..."
    />
  );
};

export default SearchFilter;
