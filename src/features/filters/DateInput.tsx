import React from "react";
import styles from "./Filters.module.scss";

type IProps = {
  value: string;
  onChange: (newValue: string) => void;
};

const DateInput = ({ value, onChange }: IProps) => {
  return (
    <input
      className={styles.input}
      type="date"
      value={value}
      onChange={(e) => onChange(e.target.value)}
    />
  );
};

export default DateInput;
