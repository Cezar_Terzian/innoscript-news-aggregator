import React from "react";
import ReactSelect from "react-select";

type Option = {
  label: string;
  value: string;
};

type IProps = {
  value: string;
  onChange: (newValue: string) => void;
  options: Option[];
  label: string;
};

const MultiSelect = ({ value, onChange, options, label }: IProps) => {
  // @ts-ignore
  const handleChange = (newOptions) => {
    if (newOptions === null) {
      // @ts-ignore
      onChange([]);
      return;
    }
    onChange(newOptions);
  };

  return (
    <>
      <label style={{ fontSize: ".85em" }}>{label}</label>
      <ReactSelect
        // value={options.find((option) => option.value === value) ?? ""}
        // onChange={(option) => onChange(option?.value)}

        isMulti
        // @ts-ignore
        options={options}
        onChange={handleChange}
        hideSelectedOptions
        defaultValue={value}
      />
    </>
  );
};

export default MultiSelect;
