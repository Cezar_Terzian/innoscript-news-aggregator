import React from "react";
import { useGetSections } from "../api";

const useGuardianFilters = () => {
  // Search
  const [search, setSearch] = React.useState("");

  // Categories
  const [selectedSectionId, setSelectedSectionId] = React.useState("");
  const { data: sectionsData } = useGetSections();
  const sections = React.useMemo(
    () =>
      sectionsData?.response?.results.map((sec: any) => ({
        label: sec.webTitle,
        value: sec.id,
      })) ?? [],
    [sectionsData]
  );

  // Dates
  const [fromDate, setFromDate] = React.useState("");
  const [toDate, setToDate] = React.useState("");

  return React.useMemo(
    () => ({
      search,
      setSearch,

      sections,
      selectedSectionId,
      setSelectedSectionId,

      fromDate,
      setFromDate,
      toDate,
      setToDate,
    }),
    [
      search,
      setSearch,
      sections,
      selectedSectionId,
      setSelectedSectionId,
      fromDate,
      setFromDate,
      toDate,
      setToDate,
    ]
  );
};

export default useGuardianFilters;
