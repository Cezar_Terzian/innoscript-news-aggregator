import React from "react";

import { useGetArticles } from "./api";
import styles from "./styles/the-guardian-styles.module.scss";
import ArticleCard from "@/components/ArticleCard";
import { formatDate } from "@/utility/date";
import MoonLoader from "react-spinners/MoonLoader";
import useGuardianFilters from "./filters/useGuardianFilters";
import SearchFilter from "../filters/SearchFilter";
import CategorySelect from "../filters/Select";
import DateInput from "../filters/DateInput";

const Articles = () => {
  const {
    search,
    setSearch,

    sections,
    selectedSectionId,
    setSelectedSectionId,

    fromDate,
    setFromDate,
    toDate,
    setToDate,
  } = useGuardianFilters();

  const { data, isLoading, isError, isSuccess } = useGetArticles({
    page: 1,
    q: search,
    section: selectedSectionId,

    from: fromDate,
    to: toDate,
  });

  return (
    <div className={styles.section}>
      <div>
        <h6 style={{ marginBottom: ".25rem" }}>Filters</h6>
        <SearchFilter onChange={setSearch} />
        <div style={{ minHeight: "1rem" }}></div>
        <CategorySelect
          label="Section"
          value={selectedSectionId}
          onChange={setSelectedSectionId}
          options={sections}
        />

        <div style={{ minHeight: "1rem" }}></div>
        <label>From</label>
        <DateInput value={fromDate} onChange={setFromDate} />
        <label>To</label>
        <DateInput value={toDate} onChange={setToDate} />
      </div>
      {isLoading && (
        <div className={styles.loading}>
          <MoonLoader size={42} />
        </div>
      )}
      {isError && (
        <div className={styles.loading}>
          <p style={{ color: "red" }}>An Error Occured</p>
        </div>
      )}
      {isSuccess && (
        <div>
          <h6 style={{ marginBottom: ".25rem" }}>Articles - The Guardian</h6>
          <div className={styles.articles_container}>
            {data?.response?.results?.map((article: any) => (
              <ArticleCard
                key={article.id}
                title={article.webTitle}
                date={formatDate(article.webPublicationDate)}
                category={article.sectionName}
                link={article.webUrl}
              />
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default Articles;
