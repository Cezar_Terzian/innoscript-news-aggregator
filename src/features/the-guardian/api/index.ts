import client from "@/api/client";
import { useQuery } from "@tanstack/react-query";

const defaultPageSize = 45;
const API_KEY = process.env.NEXT_PUBLIC_THE_GUARDIAN_KEY!;

const ENDPOINT = {
  ALL: "https://content.guardianapis.com/search",
  SECTIONS: "https://content.guardianapis.com/sections",
};

type Params = {
  page: number;
  q?: string;
  section?: string;
  from?: string;
  to?: string;
};

export const useGetArticles = (params: Params) => {
  return useQuery({
    queryKey: ["THE_GUARDIAN_ARTICLES", params],
    queryFn: async () => {
      const { data } = await client.get(ENDPOINT.ALL, {
        params: {
          ["api-key"]: API_KEY,
          ["show-elements"]: "image",
          ["page-size"]: defaultPageSize,
          page: params.page,
          q: params.q,
          section: params.section || null,
          ["from-date"]: params.from || null,
          ["to-date"]: params.to || null,
        },
      });
      return data;
    },
  });
};

export const useGetSections = () => {
  return useQuery({
    queryKey: ["THE_GUARDIAN_SECTIONS"],
    queryFn: async () => {
      const { data } = await client.get(ENDPOINT.SECTIONS, {
        params: {
          ["api-key"]: API_KEY,
        },
      });
      return data;
    },
  });
};
