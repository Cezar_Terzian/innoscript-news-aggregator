import React from "react";

import styles from "./Footer.module.scss";
import { APP_NAME } from "@/configs/globals";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.copyright_container}>
        <p>
          {APP_NAME} |{" "}
          <a
            href="https://www.linkedin.com/in/cezar-terzian/"
            rel="noreferrer noopener"
            target="_blank"
          >
            Cezar Terzian
          </a>
        </p>
      </div>
    </footer>
  );
};

export default Footer;
