import React from "react";
import styles from "./ArticleCard.module.scss";
import defaultImg from "@/assets/images/default.jpg";
import { textMaxLength } from "@/utility/text";

type IProps = {
  title: string;
  image?: string;
  date: string;
  category?: string;
  link?: string;
};

const ArticleCard = ({ title, image, date, category, link }: IProps) => {
  return (
    <div className={styles.card}>
      <div
        className={styles.img}
        style={{ backgroundImage: `url(${image ?? defaultImg.src})` }}
      ></div>

      <div className={styles.content}>
        <p className={styles.title}>{textMaxLength(title, 50)}</p>
        <div className={styles.category_date_container}>
          {category ? <p className={styles.category}>{category}</p> : <p></p>}
          <p className={styles.date}>{date}</p>
        </div>
        {link && (
          <div className={styles.link_container}>
            <a
              className={styles.link}
              href={link}
              target="_blank"
              rel="noreferrer noopener"
            >
              See Details
            </a>
          </div>
        )}
      </div>
    </div>
  );
};

export default ArticleCard;
