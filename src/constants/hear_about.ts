export const HEAR_ABOUT = [
  "Furniture Store",
  "Facebook",
  "Kijiji",
  "From a Friend",
  "Other Social Media",
  "Other",
];
export const HEAR_ABOUT_AS_OPTIONS = HEAR_ABOUT.map((i) => ({
  label: i,
  value: i,
}));
