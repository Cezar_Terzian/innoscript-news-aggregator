export const T_SHIRT_SIZES = [
  "Small",
  "Medium",
  "Large",
  "X-Large",
  "XX-Large",
  "XXX-Large",
];
export const T_SHIRT_SIZES_AS_OPTIONS = T_SHIRT_SIZES.map((i) => ({
  label: i,
  value: i,
}));
