export enum ROLE {
  DRIVER = "driver",
  LABOR = "labor",
  USER = "user",
}
