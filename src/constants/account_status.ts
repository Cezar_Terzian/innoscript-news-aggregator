export enum ACCOUNT_STATUS {
  REGISTRATION_INITIATED = "REGISTRATION_INITIATED",
  EMAIL_VERIFICATION_PENDING = "EMAIL_VERIFICATION_PENDING",
  EMAIL_VERIFIED = "EMAIL_VERIFIED",
  ADMIN_APPROVAL_PENDING = "ADMIN_APPROVAL_PENDING",
  ACCOUNT_PARTIALLY_APPROVED = "ACCOUNT_PARTIALLY_APPROVED",
  DOCUMENTS_UPLOAD_PENDING = "DOCUMENTS_UPLOAD_PENDING",
  DOCUMENTS_REVIEW_PENDING = "DOCUMENTS_REVIEW_PENDING",
  DOCUMENTS_APPROVED = "DOCUMENTS_APPROVED",
  DOCUMENTS_REJECTED = "DOCUMENTS_REJECTED",
  ACCOUNT_APPROVED = "ACCOUNT_APPROVED",
  ACCOUNT_REJECTED = "ACCOUNT_REJECTED",
}
