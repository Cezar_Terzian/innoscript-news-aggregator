export const MOVE_TYPE = {
  HEAVY: "HEAVY",
  LIGHT: "LIGHT",
} as const;

export enum MOVE_TYPE_ENUM {
  HEAVY = "HEAVY",
  LIGHT = "LIGHT",
}
