export const PROVINCES = ["Alberta", "British Columbia"];

export const PROVINCES_AS_OPTIONS = PROVINCES.map((i) => ({
  label: i,
  value: i,
}));
