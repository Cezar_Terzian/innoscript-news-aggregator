export const METROPOLITAN_AREAS = ["Calgary"];

export const METROPOLITAN_AREAS_AS_OPTIONS = METROPOLITAN_AREAS.map((i) => ({
  label: i,
  value: i,
}));
