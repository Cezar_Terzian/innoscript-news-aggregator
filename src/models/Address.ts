export default interface Address {
  city: string;
  street: string;
  province: string;
  appartment_or_unit_number?: string;
  postal_code: string;
}
