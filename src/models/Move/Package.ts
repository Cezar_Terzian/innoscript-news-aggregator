type MovePackage = {
  id: number;
  name: string;
  description: string;
  price: number;
};

export default MovePackage;
