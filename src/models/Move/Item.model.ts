type MoveItem = {
  id: number;
  item_category_id: number;
  name: string;
  unit_price: string;
};

export default MoveItem;
