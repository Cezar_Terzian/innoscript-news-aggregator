export default interface Vehicle {
  Make: string;
  Model: string;
  Year: string;
  Type: string;
}
