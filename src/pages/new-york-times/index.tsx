import React from "react";

import styles from "@/styles/new-york-times.module.scss";

import HERO_IMG from "@/assets/images/new-york-times.jpeg";

import Hero from "@/components/Hero";

import { NextPageWithLayout } from "@/layout/types";
import MainLayout from "@/layout/MainLayout";
import Articles from "@/features/new-york-times/Articles";
import DocumentTitle from "@/components/meta/DocumentTitle";

const NewYorkTimesPage: NextPageWithLayout = () => {
  return (
    <>
      <DocumentTitle title="New York Times" />

      <Hero
        backgroundImageSrc={HERO_IMG.src}
        containerClassName={styles.hero_container}
      >
        <></>
      </Hero>

      <main className="page__content">
        <Articles />
      </main>
    </>
  );
};

NewYorkTimesPage.getLayout = (page: React.ReactElement) => {
  return <MainLayout>{page}</MainLayout>;
};

export default NewYorkTimesPage;
