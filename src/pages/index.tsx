import React from "react";

import { NextPageWithLayout } from "@/layout/types";
import MainLayout from "@/layout/MainLayout";
import Articles from "@/features/news-api/Articles";

const Home: NextPageWithLayout = () => {
  return (
    <>
      <main className="page__content">
        <Articles />
      </main>
    </>
  );
};

Home.getLayout = (page: React.ReactElement) => {
  return <MainLayout>{page}</MainLayout>;
};

export default Home;
