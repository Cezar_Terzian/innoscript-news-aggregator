import React from "react";

import styles from "@/styles/the-guardian.module.scss";

import HERO_IMG from "@/assets/images/the-guardian.webp";

import Hero from "@/components/Hero";

import { NextPageWithLayout } from "@/layout/types";
import MainLayout from "@/layout/MainLayout";
import Articles from "@/features/the-guardian/Articles";
import DocumentTitle from "@/components/meta/DocumentTitle";

const TheGuardianPage: NextPageWithLayout = () => {
  return (
    <>
      <DocumentTitle title="The Guardian" />

      <Hero
        backgroundImageSrc={HERO_IMG.src}
        containerClassName={styles.hero_container}
      >
        <></>
      </Hero>

      <main className="page__content">
        <Articles />
      </main>
    </>
  );
};

TheGuardianPage.getLayout = (page: React.ReactElement) => {
  return <MainLayout>{page}</MainLayout>;
};

export default TheGuardianPage;
