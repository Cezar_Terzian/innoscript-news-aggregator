import AppHead from "@/components/meta/AppHead";
import "@/styles/globals.scss";

import { QueryClientProvider } from "@tanstack/react-query";
import queryClient from "@/configs/queryClient";
// import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

import type { AppProps } from "next/app";
import { NextPageWithLayout } from "@/layout/types";

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

export default function App({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page);

  return (
    <QueryClientProvider client={queryClient}>
      <AppHead />

      {getLayout(<Component {...pageProps} />)}
      {/* <ReactQueryDevtools initialIsOpen={false} /> */}
    </QueryClientProvider>
  );
}
