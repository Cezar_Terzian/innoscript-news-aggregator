export const EMAIL_VERIFICATION_ENDPOINTS = {
  VERIFY_EMAIL: `/api/email/verify`,
  SEND_VERIFICATION_CODE: `/api/email/send-verification-code`,
} as const;
