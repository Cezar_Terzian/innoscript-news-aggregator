export const REGISTER_ENDPOINTS = {
  REGISTER_USER: `/api/register/user`,
  REGISTER_DRIVER: `/api/register/driver`,
  REGISTER_LABOR: `/api/register/labor`,
} as const;
