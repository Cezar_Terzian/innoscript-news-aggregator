export type RegisterUserType = {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  password: string;
  password_confirmation: string;

  hear_about_us: string;
};
