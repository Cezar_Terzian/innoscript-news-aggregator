export type Page<TData> = {
  data: TData[];
  current_page: number;
  last_page: number;
};
